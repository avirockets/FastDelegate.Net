﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace FastDelegate.Net {

    // ReSharper disable once UnusedMember.Global
    public static class MethodInfoExtensions {

        private static Func<object, object[], object> CreateForNonVoidInstanceMethod(MethodInfo method, bool ignoreRef) {

            ParameterInfo[] parameters = method.GetParameters();
            if (!ignoreRef && parameters.Any(info => info.ParameterType.IsByRef)) {
                return (o, objects) => CallMethodWithRefs(o, method, objects);
            }

            ParameterExpression instanceParameter = Expression.Parameter(typeof(object), "target");
            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

            MethodCallExpression call = Expression.Call(
                // ReSharper disable once AssignNullToNotNullAttribute
                Expression.Convert(instanceParameter, method.DeclaringType),
                method,
                CreateParameterExpressions(parameters, argumentsParameter)
            );

            Expression<Func<object, object[], object>> lambda
                = Expression.Lambda<Func<object, object[], object>>(
                    Expression.Convert(call, typeof(object)),
                    instanceParameter,
                    argumentsParameter
                );

            return lambda.Compile();
        }

        private static Func<object[], object> CreateForNonVoidStaticMethod(MethodInfo method, bool ignoreRef) {

            ParameterInfo[] parameters = method.GetParameters();
            if (!ignoreRef && parameters.Any(info => info.ParameterType.IsByRef)) {
                return objects => CallMethodWithRefs(null, method, objects);
            }

            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

            MethodCallExpression call = Expression.Call(
                method,
                CreateParameterExpressions(parameters, argumentsParameter)
            );

            Expression<Func<object[], object>> lambda
                = Expression.Lambda<Func<object[], object>>(
                    Expression.Convert(call, typeof(object)),
                    argumentsParameter
                );

            return lambda.Compile();
        }

        private static Action<object, object[]> CreateForVoidInstanceMethod(MethodInfo method, bool ignoreRef) {

            ParameterInfo[] parameters = method.GetParameters();
            if (!ignoreRef && parameters.Any(info => info.ParameterType.IsByRef)) {
                return (o, objects) => CallMethodWithRefs(o, method, objects);
            }

            ParameterExpression instanceParameter = Expression.Parameter(typeof(object), "target");
            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

            MethodCallExpression call = Expression.Call(
                // ReSharper disable once AssignNullToNotNullAttribute
                Expression.Convert(instanceParameter, method.DeclaringType),
                method,
                CreateParameterExpressions(parameters, argumentsParameter)
            );

            Expression<Action<object, object[]>> lambda
                = Expression.Lambda<Action<object, object[]>>(
                    call,
                    instanceParameter,
                    argumentsParameter
                );

            lambda.Compile();

            return lambda.Compile();
        }

        private static Action<object[]> CreateForVoidStaticMethod(MethodInfo method, bool ignoreRef) {

            ParameterInfo[] parameters = method.GetParameters();
            if (!ignoreRef && parameters.Any(info => info.ParameterType.IsByRef)) {
                return objects => CallMethodWithRefs(null, method, objects);
            }

            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

            MethodCallExpression call = Expression.Call(
                method,
                CreateParameterExpressions(parameters, argumentsParameter)
            );

            Expression<Action<object[]>> lambda =
                Expression.Lambda<Action<object[]>>(
                    call,
                    argumentsParameter
                );

            return lambda.Compile();
        }

        private static Expression[] CreateParameterExpressions(ParameterInfo[] parameters, Expression argumentsParameter)
            => parameters.Select(
                    (parameter, index) => Expression.Convert(
                        Expression.ArrayIndex(argumentsParameter, Expression.Constant(index)),
                        // ReSharper disable once AssignNullToNotNullAttribute
                        parameter.ParameterType.IsByRef ? parameter.ParameterType.GetElementType() : parameter.ParameterType
                    )
                )
                .Cast<Expression>()
                .ToArray();

        private static object CallMethodWithRefs(object instance, MethodInfo info, object[] parameters)
            => info.Invoke(instance, parameters);

        /// <summary>
        /// Creates a lambda that calls given method. It will use reflection to invoke method if it has any reference parameters
        /// </summary>
        /// <param name="method">the method</param>
        /// <returns></returns>
        public static Func<object, object[], object> Bind(this MethodInfo method) => method.Bind(false);

        /// <summary>
        /// Creates a lambda that calls given method
        /// </summary>
        /// <param name="method">the method</param>
        /// <param name="ignoreRef">when false, it will use reflection to invoke method if it has any reference parameters;
        /// when true, it will ignore reference parameters (they will not be set!!!) - use at own risk</param>
        /// <returns></returns>
        public static Func<object, object[], object> Bind(this MethodInfo method, bool ignoreRef) {
            if (method.IsStatic) {
                if (method.ReturnType == typeof(void)) {
                    Action<object[]> wrapped = CreateForVoidStaticMethod(method, ignoreRef);
                    return (target, parameters) => {
                        wrapped(parameters);
                        return (object) null;
                    };
                } else {
                    Func<object[], object> wrapped = CreateForNonVoidStaticMethod(method, ignoreRef);
                    return (target, parameters) => wrapped(parameters);
                }
            }

            if (method.ReturnType == typeof(void)) {
                Action<object, object[]> wrapped = CreateForVoidInstanceMethod(method, ignoreRef);
                return (target, parameters) => {
                    wrapped(target, parameters);
                    return (object) null;
                };
            } else {
                Func<object, object[], object> wrapped = CreateForNonVoidInstanceMethod(method, ignoreRef);
                return wrapped;
            }
        }

    }

}