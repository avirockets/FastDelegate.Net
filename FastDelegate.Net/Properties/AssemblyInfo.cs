﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FastDelegate.Net")]
[assembly: AssemblyDescription("A replacement for .Net's slow Delegate.DynamicInvoke")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: ComVisible(false)]

[assembly: AssemblyCompany("coder0xff")]
[assembly: AssemblyProduct("FastDelegate.Net")]
[assembly: AssemblyCopyright("Copyright © Brent W. Lewis (coder0xff) 2015")]

[assembly: Guid("cba1dcef-dabd-4fd6-aad6-16b20bfb11bc")]

[assembly: AssemblyVersion("1.0.2.0")]
[assembly: AssemblyFileVersion("1.0.2.0")]
[assembly: AssemblyInformationalVersion("1.0.2.0")]